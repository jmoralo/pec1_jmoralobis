var gulp=require('gulp'),
    del=require('del'),
    browserSync=require('browser-sync').create(),
    concat =require('gulp-concat'),
    imagemin= require('gulp-imagemin'),
    jshint      = require('gulp-jshint'),stylish= require('jshint-stylish'),
    sass= require('gulp-sass'),
    sourcemaps= require('gulp-sourcemaps'),
    uglify= require('gulp-uglify');

// Definición de direcotrios origen
var srcPaths = {
    images:'src/img/',
    scripts:'src/js/',
    styles:'src/scss/',
    files: 'src/'
};

// Definición de directorios destino
var distPaths = {
    images:   'dist/img/',
    scripts:  'dist/js/',
    styles:   'dist/css/',
    files:    'dist/'
};


// Limpieza del directorio dist
gulp.task('clean', function(cb) {
    del([ distPaths.files+'*.html', distPaths.images+'**/*', distPaths.scripts+'*.js', distPaths.styles+'*.css'], cb);
    cb();
});


// Copia de los cambios en los ficheros html en el directorio dist.
gulp.task('html', function() {
    return gulp.src([srcPaths.files+'*.html'])
        .pipe(gulp.dest(distPaths.files))
        .pipe(browserSync.stream());
});

gulp.task ('carpetas',()=>
gulp.src([srcPaths.files+'vendor',
            srcPaths.files+'data'])
    .pipe(gulp.dest(distPaths.files))
);

gulp.task ('vendor',()=>
    gulp.src([srcPaths.files+'vendor/**/*'])
        .pipe(gulp.dest(distPaths.files+'vendor/'))
);
gulp.task ('data',()=>
    gulp.src([srcPaths.files+'data/**/*'])
        .pipe(gulp.dest(distPaths.files+'data/'))
);

gulp.task('copy',gulp.series('carpetas','vendor','data','html'));

/*
* Procesamiento de imágenes para comprimir / optimizar las mismas.
*/
gulp.task('imagemin', function() {
    return gulp.src([srcPaths.images+'**/*'])
        .pipe(imagemin({
            progressive: true,
            interlaced: true,
            svgoPlugins: [{removeUnknownsAndDefaults: false}, {cleanupIDs: false}]
        }))
        .pipe(gulp.dest(distPaths.images))
        .pipe(browserSync.stream());
});


/*
* Procesamiento de ficheros SCSS para la generación de los ficheros
* CSS correspondientes. Los sourcemaps en este caso se generan dentro
* del propio fichero.
 */
gulp.task('css', function() {
    return gulp.src([srcPaths.styles+'base.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sourcemaps.write())
.pipe(gulp.dest(distPaths.styles))
        .pipe(browserSync.stream());
});


/*
 * Procesamiento de ficheros JS mediante JSHint para detección de errores.
* Este proceso es previo al tratamiento de los ficheros JS para la
* obtención del fichero concatenado y minificado.
 */
gulp.task('lint', function() {
    return gulp.src([srcPaths.scripts+'**/*.js'])
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});



/*
* Procesamiento de ficheros JS para la generación de un fichero
* final único y minificado. Los sourcemaps se generan en una
* carpeta independiente en vez de en el propio fichero.
*/
gulp.task('js_task', function() {
    return gulp.src([srcPaths.scripts+'contact.js', srcPaths.scripts+'contactList.js',srcPaths.scripts+'main.js'])

        .pipe(sourcemaps.init())
        .pipe(concat('all.min.js', ))
        .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest(distPaths.scripts))
        .pipe(browserSync.stream());
});
gulp.task('js', gulp.series('lint', 'js_task'));



/*
 * Se crean los watchers para procesar los cambios que se
 * puedan producir en los archivos sensibles para el proyecto.
 */
gulp.task('watch', function () {

    gulp.watch(srcPaths.files+'*.html', gulp.series('html',browserSync.reload));
    gulp.watch(srcPaths.scripts+'**/*.js', gulp.series('js',browserSync.reload));

});


/*
 * Tarea para lanzar el proceso de servidor mediante BrowserSync.
* Antes de comenzar la propia tarea ejecuta las tareas de las que tiene
* dependencia: html, imagemin, css y js necesarias para disponer
 * del proyecto en dist, ya que cada vez que se lanza gulp, se hace una
* limpieza de dicho directorio.
 *
* En este caso se trabaja con un servidor local mediante un proxy
* y se define la ruta de partida, así como los navegadores a lanzar
* en caso de estar disponibles en el equipo.
 */

// gulp.task('serve',
//     gulp.series('copy','imagemin','css','js',
//     function() {
//     browserSync.init({
//         server: 'dist',
//         logLevel: "info",
//         browser: ["chrome", "Firefox"],
//         startPath: 'index.html'
//     })
//         gulp.watch(srcPaths.files+'*.html', ['html']).on('change',browserSync.reload),
//             gulp.watch(srcPaths.scripts+'**/*.js', ['js']).on('change',browserSync.reload),
//         gulp.watch(srcPaths.styles+'**/*.scss',['css'])
// }));
gulp.task('all',gulp.series('copy','imagemin','css','js'));
gulp.task('serve',()=>{
            browserSync.init({
                server: 'dist',
                logLevel: "info",
                browser: ["chrome", "Firefox"],
                startPath: 'index.html'
            });
            gulp.watch(srcPaths.files+'*.html',gulp.series('html'));
            gulp.watch(srcPaths.styles+'**/*.scss',gulp.series('css'));
            gulp.watch(srcPaths.scripts+'**/*.js', gulp.series('js')).on('change',browserSync.reload);




        });
/*
 limpia el directorio destino
* y lanza la tarea de servidor./*
* Definción de la tarea por defecto que en este caso
*/

gulp.task('default', gulp.series('clean','all','serve'));




